package com.lucaongaro.minhash

import scala.collection.immutable.SortedSet
import scala.concurrent._

trait Engine[S, E] {
  implicit val executionContext: ExecutionContext
  implicit val hashable: Hashable[E]

  /**
   * Adds an element to a set
   */
  def addElementToSet( setId: S, setElem: E ): Future[Unit]

  /**
   * Adds an element to multiple sets
   */
  def addElementToSets( setIds: Seq[S], setElem: E ): Future[Unit] =
    for {
      f <- Future.traverse( setIds ) { setId => addElementToSet( setId, setElem ) }
    } yield ()

  /**
   * Adds multiple elements to a set
   */
  def addElementsToSet( setId: S, setElems: Seq[E] ): Future[Unit] =
    for {
      f <- Future.traverse( setElems ) { setElem => addElementToSet( setId, setElem ) }
    } yield ()

  /**
   * Retrieves the sets that are most similar to the given set
   */
  def findNeighborsOf( setId: S, limit: Int = 10, threshold: Double = 0.1 )
    ( implicit o: Ordering[Neighbor[S]] ): Future[SortedSet[Neighbor[S]]]
}

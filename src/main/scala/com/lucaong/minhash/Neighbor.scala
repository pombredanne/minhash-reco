package com.lucaongaro.minhash

case class Neighbor[S]( setId: S, similarity: Float )

object Neighbor {
  implicit def ordering[S]( implicit ev: S => Ordered[S] ) =
    Ordering.fromLessThan[Neighbor[S]] { ( a, b ) =>
      if ( a.similarity == b.similarity )
        a.setId > b.setId
      else
        a.similarity > b.similarity
    }
}

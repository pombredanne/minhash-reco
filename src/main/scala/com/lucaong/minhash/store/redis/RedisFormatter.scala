package com.lucaongaro.minhash.store.redis

import _root_.redis.ByteStringFormatter
import akka.util.{ ByteStringBuilder, ByteString }
import com.lucaongaro.minhash._

trait RedisFormatter[T] extends ByteStringFormatter[T] {
  def toKey( thing: T ): String
}

object RedisFormatter {
  implicit object IntSeqByteStringFormatter extends RedisFormatter[Seq[Int]] {
    implicit val byteOrder = java.nio.ByteOrder.BIG_ENDIAN

    def serialize( seq: Seq[Int] ) = {
      val bsb = new ByteStringBuilder()
      seq.foreach( bsb putInt _ )
      bsb.result()
    }

    def deserialize( bs: ByteString ) =
      bs.grouped(4).map( _.iterator.getInt ).toList

    def toKey( seq: Seq[Int] ) = seq.mkString(".")
  }

  implicit object SignatureByteStringFormatter extends RedisFormatter[Signature] {
    implicit val byteOrder = java.nio.ByteOrder.BIG_ENDIAN

    def serialize( signature: Signature ) = {
      val bsb = new ByteStringBuilder()
      signature.hashes.foreach( bsb putInt _ )
      bsb.result()
    }

    def deserialize( bs: ByteString ) =
      Signature( bs.grouped(4).map( _.iterator.getInt ).toList )

    def toKey( signature: Signature ) = signature.hashes.mkString(".")
  }

  implicit object StringByteStringFormatter extends RedisFormatter[String] {
    implicit val byteOrder = java.nio.ByteOrder.BIG_ENDIAN

    def serialize( str: String ) = ByteString( str )

    def deserialize( bs: ByteString ) = bs.decodeString("UTF-8")

    def toKey( str: String ) = str
  }

  implicit object LongByteStringFormatter extends RedisFormatter[Long] {
    implicit val byteOrder = java.nio.ByteOrder.BIG_ENDIAN

    def serialize( n: Long ) = {
      val bsb = new ByteStringBuilder()
      bsb putLong n
      bsb.result()
    }

    def deserialize( bs: ByteString ) = bs.iterator.getLong

    def toKey( n: Long ) = n.toString
  }

  implicit object IntByteStringFormatter extends RedisFormatter[Int] {
    implicit val byteOrder = java.nio.ByteOrder.BIG_ENDIAN

    def serialize( n: Int ) = {
      val bsb = new ByteStringBuilder()
      bsb putInt n
      bsb.result()
    }

    def deserialize( bs: ByteString ) = bs.iterator.getInt

    def toKey( n: Int ) = n.toString
  }
}

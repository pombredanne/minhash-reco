package com.lucaongaro.minhash.store.redis

import _root_.redis._
import _root_.redis.actors.ReplyErrorException
import _root_.redis.api.scripting._
import _root_.redis.protocol._
import scala.concurrent.{ Future, ExecutionContext }

class RedisClientExt( redis: RedisClient )( implicit ec: ExecutionContext ) {

  def compareAndSwap[K, A]( key: K, compare: A, swap: A )
    ( implicit ks: ByteStringSerializer[K], as: ByteStringSerializer[A] ): Future[RedisReply] =
    evalshaOrEval( RedisClientExt.compareAndSwapLua, Seq( key ), Seq( compare, swap ) )

  def evalshaOrEval[K, A]( rscript: RedisScript, keys: Seq[K], args: Seq[A] )
    ( implicit ks: ByteStringSerializer[K], as: ByteStringSerializer[A] ): Future[RedisReply] =
    evalsha( rscript.sha1, keys, args ).recoverWith({
      case ReplyErrorException( msg ) if msg.startsWith("NOSCRIPT") => eval( rscript.script, keys, args )
    })

  def evalsha[K, A]( sha1: String, keys: Seq[K] = Seq.empty[K], args: Seq[A] = Seq.empty[A] )
    ( implicit ks: ByteStringSerializer[K], as: ByteStringSerializer[A] ): Future[RedisReply] =
    redis.send( Evalsha( sha1, keys, args ) )

  def eval[K, A]( script: String, keys: Seq[K] = Seq.empty[K], args: Seq[A] = Seq.empty[A] )
    ( implicit ks: ByteStringSerializer[K], as: ByteStringSerializer[A] ): Future[RedisReply] =
    redis.send( Eval( script, keys, args ) )
}

object RedisClientExt {
  val compareAndSwapLua = RedisScript("""
    local cur = redis.call('get',KEYS[1])
    if cur == ARGV[1] or cur == false then
      return redis.call('set',KEYS[1],ARGV[2])
    else
      return 0
    end
    """)
}

package com.lucaongaro.minhash

import scala.language.implicitConversions
import scala.util.hashing.{ MurmurHash3 => MM3 }

case class Signature( hashes: Seq[Hash] ) {
  def toBuckets( implicit opts: MinHashOptions ): Buckets =
    hashes.view.grouped( opts.hashesPerBand ).take( opts.numOfBands ).map( _ reduce ( _ ^ _ ) )
      .zipWithIndex.map( _.swap ).toMap

  def similarity( other: Signature )( implicit opts: MinHashOptions ): Float =
    ( hashes zip other.hashes ).count { case (a, b) => a == b } / opts.numOfHashes.toFloat

  def toSeq = hashes

  def updateWith( other: Signature ): Signature =
    Signature( ( hashes zip other.hashes ).map { case (a, b) => a min b } )
}

object Signature {
  def calculate[T]( setElem: T, seeds: Seq[Int] )( implicit h: Hashable[T] ): Signature =
    new Signature( h.hashes( setElem, seeds ) )

  implicit def signatureToSeq( signature: Signature ) = signature.toSeq
  implicit def seqToSignature( seq: Seq[Int] ) = Signature( seq )
}
